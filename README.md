# [drone-ci-configs](https://git.dotya.ml/dotya.ml/drone-ci-configs)

this repo holds
* `docker-compose.yml` for [drone.dotya.ml instance](https://drone.dotya.ml) of
  [Drone CI](https://drone.io)
* drone-related env files (`server.env`, `runner-docker.env`)
* `systemd` *Unit* file to easily start/stop the instance service and `systemd`
  *Slice* file to control its resource usage

### prepare environment

```bash
mkdir -pv /etc/drone /etc/systemd/system/drone.service.d
```
* unless running with unprivileged user, copy `drone.service` and `drone.slice`
  files to `/etc/systemd/system` and `override.conf` into
  `/etc/systemd/system/drone.service.d`

* reload `systemd` daemon
  ```bash
  systemctl daemon-reload
  ```

* let `drone.service` start automatically on system startup and start the
  service immediately
  ```bash
  systemctl enable --now drone.service
  ```

alternatively, copy the folder structure `etc` to your `/etc`

### notes

* using this set-up a reverse proxy (such as `nginx`) is also needed to forward
  traffic to and from `drone` server that listens exclusively on localhost
* this set-up works with Gitea in mind, although it shouldn't be too hard to
  change the configs to work with other git providers (see `server.env` file)
